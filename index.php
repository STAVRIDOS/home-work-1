<?php
//#1
$name = 'Bob';
$years = 20;
echo "<p>Hello $name! You are $years years old.</p>";


//#2
$x = 10;
$y = 11.5;
$sum = $x + $y;

echo "<p>The sum of $x and $y is $sum</p>";

//#3
// const

//#4
$name = "Василий";
$admin = $name;

echo $admin;

//#5
$planet = "";
$visitorName = "Петя";

//#6
$x = 5;

//#7 (a && b)
$target = 55;
echo ($target === 55) ? '<p>Верно</p>' : '<p>Неверно</p>';

//#7 c
$lowTarget = 5;
echo ($target <= 5) ? '<p>Меньше или равно пять</p>' : '<p>Неверно</p>';

//#7 d
$minute = 16;
$result = "";
if($minute <= 15){
    $result = '1';
}else if($minute <= 30 && $minute > 15){
    $result = '2';
}else if($minute <= 45 && $minute > 30){
    $result = '3';
}else{
    $result = '4';
}

echo "<p>$result</p>";

//#7 e
$x = 15;
$y = 25;
echo (($x <= 3 || $x > 10) && ($y >= 2 && $y < 24) ) ? "<p>True</p>" : 'False</p>';

//#7 f
$string = "91222";
$numbers_1 = 0;
$numbers_2 = 0;
for($i = 0; $i < $string.length;$i++){
    ($i < 2) ? $numbers_1 += $string[$i] : $numbers_2 += $string[$i];
};
echo ($numbers_1 === $numbers_2) ? '<p>да' : '<p>нет</p>';

//#8 a
$language = 'ru_RU';
$languageArray = ['en_GB','ru_RU'];
$result = "";
($language === 'en_GB') ? $result = $languageArray[0] : $result = $languageArray[1];
print_r($result);


//#9 a
$arr = ['World','!', 'Hello'];

echo "<p>".$arr[2].$arr[0].$arr[1]."</p>";

//#10
$responseStatuses = [
    100 => 'Сообщения успешно приняты к отправке',
    105 => 'Ошибка в формате запроса',
    110 => 'Неверная авторизация',
    115 => 'Недопустимый IP-адрес отправителя',
];

echo "<p>".$responseStatuses[105]."</p>";
echo "<p>".$responseStatuses[110]."</p>";

//#11
$fruits = ['apple' => 3, 'orange' => 5, 'pear' => 10];
echo "<p>".$fruits['apple'] + $fruits['orange']."</p>";
echo "<p>".$fruits['orange'] + $fruits['pear']."</p>";

//#12
$arr = [
    'cms' => ['joomla', 'wordpress', 'drupal'],
    'colors' => ['blue'=>'голубой', 'red'=>'красный', 'green'=>'зеленый'],
];
$newArray = [];

for($i = 0; $i < 2; $i++){
    array_push($newArray, $arr['cms'][$i]);
    echo "<p>".$arr['colors'][$i]."</p>";
}
echo join(',', $newArray);
echo "<p>".$arr['colors']['green']."</p>";
echo "<p>".$arr['cms'][2]."</p>";
